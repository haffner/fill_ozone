
; overplot V8 TOMS Raman corrections from J. Joiner

x=[[312.5, 0.175],   [317.35,-0.689],   [321.7, 0.],$
   [331.2, 0.196],   [340.,-0.141],   [378.5, 0.382]]

tvlct,200,50,50,1
t=2*[0:41]/40.*!pi
usersym, cos(t), sin(t),/fill
oplot, x[0,*], x[1,*], psym=8, symsize=.8,color=1
legend, /top,/left, psym=[3,3,8], ["","","TOMS corrections"],box=0,color=1
end
  
