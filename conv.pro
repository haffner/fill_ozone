function conv, y, x, w
  yout=fltarr(x.length)
  for i=0, x.length-1 do begin
     b=1-abs(x-x[i])/w > 0
     yout[i]=total(b*y)/total(b)
  endfor
  return, yout 
end