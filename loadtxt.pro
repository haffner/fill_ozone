; loadtxt.pro
function loadtxt, file, skiprows=skiprows

  sample=strarr(1)

  openr,71,file
  
  if keyword_set(skiprows) then begin
     hdr=strarr(skiprows)
     readf,71,hdr
  endif else begin
     skiprows=0
  endelse
  
  readf,71,sample
  close,71

  junk=strsplit(sample,count=ncol)
  nrow=file_lines(file)-skiprows
  data=fltarr(ncol,nrow)

  openr,71,file
  if skiprows gt 0 then readf,71,hdr
  readf,71,data
  close,71

  return,data

end
