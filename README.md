11 Aug 2019

Code, data, and results for Ring correction to OMI elastic radiance calculations.
Dave Haffner (SSAI)

IDL code:
conv_omi.pro will convolve, plot, and report Raman filling-in corrections to elastic radiance
corrections are in percent, i.e. apply correction as

  (1+cor/100)*rade

where rade is the elastic forward modeled (RTM) radiance, simulated without Ring effect.
cor is calcualted as (radt_conv/rade_conv-1.0)*100 where radt is total radiance
(elastic + inelastic), rade is elastic radiance, and "_conv" denotes the quantity
was convolved after weighting by solar flux (sol). Convolution is done with triangular
and Gaussian kernel for comparison, assuming 0.42 nm FHWM for both.

results (corrections) for 312.34 and 317.35 nm at OMI resolution (UV-2) are in lrrs_omi.dat  
lrrs_omi.pdf shows corrections accross the spectrum.

Monochromatic data calculated with LIDORT-RRS (LRRS)

with ozone

lrrs_mono_A005_tau0_SZA45_VZA0_307_330.dat
lrrs_mono_A005_tau0_SZA45_VZA50_307_330.dat
lrrs_mono_A005_tau0_SZA70_VZA0_307_330.dat
lrrs_mono_A005_tau0_SZA85_VZA0_307_330.dat

without ozone

lrrs_mono_A005_tau0_nooz_SZA45_VZA0_307_330.dat
lrrs_mono_A005_tau0_nooz_SZA45_VZA50_307_330.dat
lrrs_mono_A005_tau0_nooz_SZA70_VZA0_307_330.dat
lrrs_mono_A005_tau0_nooz_SZA85_VZA0_307_330.dat

Data courtesy of Alexander Vasilkov (SSAI).
