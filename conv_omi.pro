; convolve lrrs mono spectra at omi resolution=
; using gaussian and triangular slit function
; compute filling-in correction to elastic component

del=0.01 & nl=75 
lam=del*(findgen(nl*2+1)-nl)
fwhm=0.42
sig=fwhm/(2.0*sqrt(2.0*alog(2.0)))
res=exp(-lam^2/2.0/sig^2)

file='lrrs_mono_A005_tau0_SZA45_VZA0_307_330.dat'
arr=loadtxt(file)

wav=arr[0,*]
sol=arr[1,*]
rade=arr[2,*]
radt=arr[3,*]
fillw=arr[4,*]

rade_conv=convol(transpose(rade*sol),res,total(res),/center)
radt_conv=convol(transpose(radt*sol),res,total(res),/center)

fill=(radt_conv/rade_conv-1.0)*100.
; compare this with IDL> fill=(1.0-rade_conv/radt_conv)*100.

plot,wav,fill,yrange=[-3,4],xrange=[305,330],xstyle=1,/nodata,/ystyle,xtitle='Wavelength (nm)',ytitle='Correction to Elastic (%)'
xyouts,0,.98,'LRRS SZA=45 VZA=0 R=0.05 O3=350 DU FHWM='+string(fwhm,format='(F4.2)')+' nm',/normal,charsize=1
oplot,wav,fill,thick=2

x0=[312.34, 317.35]
fill_int=interpol(fill,wav,x0)

rade_conv_int=interpol(rade_conv,wav,x0)
radt_conv_int=interpol(radt_conv,wav,x0)

oplot,x0,fill_int,psym=5,thick=2

openw,10,'lrrs_omi.dat'
printf,10,'Ring corrections: '+string(fwhm,format='(F4.2)')+' nm fwhm'
printf,10,file
printf,10,'lamda(nm)  corr(% elastic)'
for i=0,1 do printf,10, x0[i], fill_int[i], format='(F6.2,2X,F9.3)'
close,10

rade_conv=conv(rade*sol,wav,fwhm)
radt_conv=conv(radt*sol,wav,fwhm)

rade_conv[where(wav le wav[0]+fwhm)]=!VALUES.F_NAN
radt_conv[where(wav gt wav[-1]-fwhm)]=!VALUES.F_NAN
fill=(radt_conv/rade_conv-1.0)*100.
oplot,wav,fill,line=1,thick=2


legend,['GAU','TRI'],line=[0,1],box=0,thick=2,charsize=1,corners=c

end
